#packages
library(stringr)
library(ggplot2)
library(randomForest)
library(tm)

#0. feladat
set.seed(1)

#1. feladat
labDF <- read.csv('labeledTrainData.tsv',header = TRUE, sep = "\t", quote = "", 
                  colClasses = c('character','factor','character'), encoding = "UTF-8")

unlabDF <- read.csv('unlabeledTrainData.tsv',header = TRUE, sep = "\t", quote = "", 
                  colClasses = c('character','character'), encoding = 'UTF-8')
#labeled
head(labDF)
sapply(labDF,typeof)
sapply(labDF,class)

nrow(labDF)
summary(labDF$sentiment)

reviewLengths <- nchar(labDF$review)
qplot(reviewLengths, geom = "histogram", col = I("grey"), fill = I('black'), main = "Distribution of review lengths", bins = 350)

plot(density(reviewLengths), ylab = 'density', xlab='review length', main='Distribution of review lengths')

#unlabeled
head(unlabDF)
sapply(unlabDF,typeof)
sapply(unlabDF,class)

nrow(unlabDF)


#2. feladat
removeHTMLTags <- function(htmlStringVector) {
  result <- gsub("<.*?>", " ", htmlStringVector)
  result <- gsub("[[:punct:]]", "", result)
  # MAGYARAZAT:
  # (1): [:punct:] --> R-beli regularis kifejezesekben az irasjeleket jeloli
  # (2): [abc] --> barmelyik karakter a felsoroltak kozul: a vagy b vagy c
  # (1) es (2) alapjan: [[:punct:]] --> barmelyik irasjel
  #
  return(tolower(result))
}

#eredetileg tartalmazo:
labDF[10,3]
unlabDF[3,2]

labDF$review <- removeHTMLTags(labDF$review)
unlabDF$review <- removeHTMLTags(unlabDF$review)

labDF[10,3]
unlabDF[3,2]

train_indices <- sample(nrow(labDF),round(nrow(labDF)*0.7))

trainDF <- labDF[train_indices,]
testDF <- labDF[-train_indices,]

#3. feladat
afinn <- read.csv('AFFIN-111.txt', sep = "\t", 
                    quote = "\"", 
                    col.names = c('word','sentiment_value'),
                    colClasses = c('character','factor'), fill = T
                    )

nrow(afinn)

qplot(afinn$sentiment_value,main = "Sentiment values' distribution",xlab = 'Sentiment value')


#3.2
documentTermMxTrain <-  apply(as.matrix
                              (trainDF$review),
                              1,
                              str_count,
                              pattern = afinn$word)

documentTermMxTrain <-  t(documentTermMxTrain)

documentTermMxTest <-  apply(as.matrix
                             (testDF$review),
                             1,
                             str_count,
                             pattern = afinn$word)

documentTermMxTest <-  t(documentTermMxTest)
# a matrixok merete:
dim(documentTermMxTrain)
dim(documentTermMxTest)

#scoreTesting <- testDF

#factorrol atallitas
afinn$sentiment_value <- as.numeric(levels(afinn$sentiment_value)[afinn$sentiment_value])
#scoreTesting$score <- documentTermMxTest %*% afinn$sentiment_value
testDF$score <- documentTermMxTest %*% afinn$sentiment_value
trainDF$score <- documentTermMxTrain %*% afinn$sentiment_value

ggplot(trainDF,  aes(score, fill = as.factor(sentiment))) + geom_density(alpha = .2)

#3.3 feladat
rf_model <- randomForest(sentiment ~ score, data = trainDF)
testDF$prediction <- predict(rf_model,testDF)


getPerformanceMeasures <- function(predictions, realValues){  # konfuzios mx meghatarozasa
  confusion_mx <- matrix(nrow = 2,ncol = 2) #1. sor: TP FP 2.sor FN TN
  
  TP <- sum(predictions == 1 & realValues == 1)
  TN <- sum(predictions == 0 & realValues == 0)
  FP <- sum(predictions == 1 & realValues == 0)
  FN <- sum(predictions == 0 & realValues == 1)
  
  P <- sum(realValues == 1)
  N <- sum(realValues == 0)
  
  confusion_mx[1,1] <- TP
  confusion_mx[1,2] <- FP
  confusion_mx[2,1] <- TN
  confusion_mx[2,2] <- FN
    # precision meghatarozasa
    precision <- TP/(TP+FP)
    # recall meghatarozasa
    recall <- TP/P
    # accuracy meghatarozasa
    accuracy <- (TP+TN)/(P+N)
    return(list(confusion_mx=confusion_mx, precision=precision, recall=recall, accuracy=accuracy))
}

measureList <- getPerformanceMeasures(testDF$prediction, testDF$sentiment)

#4. feladat
lab_neg_index <- which(trainDF$sentiment == 0)
lab_pos_index <- which(trainDF$sentiment == 1)

lab_neg_corpus <- Corpus(VectorSource(trainDF[lab_neg_index,]$review))
lab_pos_corpus <- Corpus(VectorSource(trainDF[lab_pos_index,]$review))

lab_neg_corpus <- tm_map(lab_neg_corpus, removeWords, stopwords('eng'))
lab_pos_corpus <- tm_map(lab_pos_corpus, removeWords, stopwords('eng'))

doc_term_neg <- DocumentTermMatrix(lab_neg_corpus)
doc_term_neg_reduced <- removeSparseTerms(doc_term_neg, 0.98)
doc_term_pos <- DocumentTermMatrix(lab_pos_corpus)
doc_term_pos_reduced <- removeSparseTerms(doc_term_pos, 0.98)

doc_term_neg_reduced <- as.matrix(doc_term_neg_reduced)
doc_term_pos_reduced <- as.matrix(doc_term_pos_reduced)

top_neg_words <- colnames(doc_term_neg_reduced)
top_pos_words <- colnames(doc_term_pos_reduced)

dim(doc_term_neg_reduced)
dim(doc_term_pos_reduced)
sample(top_neg_words,5)
sample(top_pos_words,5)

united_words <- setdiff(union(top_neg_words,top_pos_words),intersect(top_neg_words,top_pos_words))
sample(united_words,5)
length(united_words)


#4.2 feladat
#hozzaadott oszlop eltavolitasa
combinedDF <- rbind(trainDF,testDF[,-which(colnames(testDF) == "prediction")])

combined_corpus <- Corpus(VectorSource(combinedDF$review))
combined_corpus <- tm_map(combined_corpus, removeWords, stopwords('eng'))

doc_term_combined <- DocumentTermMatrix(combined_corpus, list(dictionary = c(united_words)))
# doc_term_combined_reduced <- removeSparseTerms(doc_term_combined, 0.98)
doc_term_combined <- as.matrix(doc_term_combined)

tf <- doc_term_combined
idf <- apply(tf[,],2,function(x) {sum(x[x>0])})
idf <- log(dim(doc_term_combined)[1]/idf)
tfidf <- t(apply(tf,1,function(x) {x*idf}))

#4.3 feladat
tfidf <- as.data.frame(tfidf)
tfidf_train <- tfidf[1:(dim(tfidf)[1]*0.7),]
tfidf_test <- tfidf[((dim(tfidf)[1]*0.7)+1):dim(tfidf)[1],]

tfidf_train[is.na(tfidf_train)] <- 0
tfidf_test[is.na(tfidf_test)] <- 0

rf_model <- randomForest(sentiment_ ~ ., data = tfidf_train, ntree = 50)
rf_model <- randomForest(x = tfidf_train, y = trainDF$sentiment, ntree = 50)
testDF$prediction_ <- predict(rf_model,tfidf_test)

getPerformanceMeasures(testDF$prediction_,testDF$sentiment)
